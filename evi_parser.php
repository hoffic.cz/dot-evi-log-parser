<?php
/**
 * Created by PhpStorm.
 * User: hoffic
 * Date: 20/02/19
 * Time: 21:08
 */

$handle = fopen("2185_2019010103.evl", "r");
if ($handle !== false) {
    while (($chunk = fgets($handle)) !== false) {
        processFileChunk($chunk);
    }

    fclose($handle);
} else {
    echo 'Je to v hajzlu, soubor nejde číst :P';
}

function processFileChunk(string $chunk)
{
    // Rozseká na nový řádky podle pole času
    $chunk = preg_replace(
        '/\[\d{4}(?:-\d{2}){2} (?:\d{2}:){2}\d{2}]/',
        "\n$0",
        $chunk
    );

    // Odstraní bordel na koncích řádků
    $chunk = preg_replace(
        '/\x00[^\x5B]*/',
        "\n",
        $chunk
    );

    // Rozdělí řádky do pole (array)
    $lines = explode("\n", $chunk);

    foreach ($lines as $line) {
        processLine($line);
    }
}

function processLine(string $line)
{
    //rozdělí řádek na itemy (item je ve formátu: klíč=hodnota)
    $items = explode(' ', $line);

    //výsledek řádku
    $parsedLine = [];

    //pro každý item
    foreach ($items as $item) {
        //rozdělí item na pole ve formátu klíč, hodnota]
        $keyAndValue = explode('=', $item);

        //pokud má item 2 prvky (je ve formátu klíč=hodnota)
        if (count($keyAndValue) == 2) {
            //přidá klíč a hodnotu do pole výsledků
            $parsedLine[$keyAndValue[0]] = $keyAndValue[1];
        }
    }

    if (!empty($parsedLine)) {
        saveLine($parsedLine);
    }
}

function saveLine(array $line) {
    print_r($line);
}
